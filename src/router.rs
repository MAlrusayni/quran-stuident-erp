use axum::{
    http::StatusCode,
    routing::{get, post},
    Json, Router,
};
use serde_json::json;
use tower_http::{cors::CorsLayer, trace::TraceLayer};

use crate::{domain, state::AppState};

pub fn axum_router(state: AppState) -> Router {
    Router::new()
        .fallback(fallback)
        .route("/health", get(health_check))
        .route("/auth/login", post(domain::session::controller::login))
        .route("/auth/sign-in", post(domain::user::controller::sign_in))
        .route("/auth/logout", post(domain::session::controller::logout))
        .route("/", get(domain::user::controller::list_users))
        .layer(TraceLayer::new_for_http())
        .layer(CorsLayer::very_permissive())
        .with_state(state)
}

pub async fn fallback() -> (StatusCode, Json<serde_json::Value>) {
    let code = StatusCode::NOT_FOUND;
    (
        code,
        Json(json!({"code": code.as_u16(), "message": "No route match"})),
    )
}

pub async fn health_check() -> &'static str {
    "health OK"
}
