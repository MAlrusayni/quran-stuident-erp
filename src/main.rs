use anyhow::Result;

#[tokio::main]
pub async fn main() -> Result<()> {
    reserve_it_for_me::run().await?;
    Ok(())
}
