use crate::domain::permission::model::Permission;
use crate::domain::user::model::User;
use diesel::prelude::*;

use serde::Serialize;

use crate::schema::{groups, groups_members, groups_permissions};

#[derive(Clone, Debug, PartialEq, Queryable, Selectable, Identifiable, Eq, Serialize)]
#[diesel(table_name = groups, primary_key(id))]
pub struct Group {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
}

#[derive(Insertable)]
#[diesel(table_name = groups)]
pub struct NewGroup<'a> {
    pub name: &'a str,
    pub description: Option<&'a str>,
}

#[derive(Identifiable, Selectable, Queryable, Associations, Debug)]
#[diesel(
    table_name = groups_members,
    belongs_to(Group),
    belongs_to(User),
    primary_key(group_id, user_id)
)]
pub struct GroupMember {
    group_id: i32,
    user_id: i32,
}

#[derive(Identifiable, Selectable, Queryable, Associations, Debug)]
#[diesel(
    table_name = groups_permissions,
    belongs_to(Group),
    belongs_to(Permission),
    primary_key(group_id, permission_id)
)]
pub struct GroupPermission {
    group_id: i32,
    permission_id: i32,
}
