use diesel::{
    dsl::{count, exists},
    insert_into,
    prelude::*,
    select,
};
use diesel_async::RunQueryDsl;

use crate::database::DbConnection;

use super::{
    error::Error,
    model::{NewUser, User},
};
use common::value_object::email::Email;

#[derive(Clone)]
pub struct UserService;

impl UserService {
    pub async fn add_user(
        &self,
        user: NewUser<'_>,
        conn: &mut DbConnection,
    ) -> Result<User, Error> {
        use crate::schema::users::{self, *};

        let is_email_used = select(exists(users::table.filter(email.eq(user.email))))
            .get_result::<bool>(conn)
            .await?;

        if is_email_used {
            return Err(Error::EmailAlreadyUsed);
        }

        insert_into(users::table)
            .values(user)
            .returning(User::as_select())
            .get_result(conn)
            .await
            .map_err(From::from)
    }

    pub async fn find_by_email(
        &self,
        value: &Email,
        conn: &mut DbConnection,
    ) -> Result<Option<User>, Error> {
        use crate::schema::users::{self, *};

        users::table
            .select(User::as_select())
            .filter(email.eq(value))
            .first::<User>(conn)
            .await
            .optional()
            .map_err(From::from)
    }

    pub async fn find_by_id(
        &self,
        value: i32,
        conn: &mut DbConnection,
    ) -> Result<Option<User>, Error> {
        use crate::schema::users::{self, *};

        users::table
            .select(User::as_select())
            .filter(id.eq(value))
            .first(conn)
            .await
            .optional()
            .map_err(From::from)
    }

    pub async fn get(
        &self,
        conn: &mut DbConnection,
        page: u32,
        page_size: u8,
    ) -> Result<Vec<User>, Error> {
        use crate::schema::users;
        users::table
            .offset((page * page_size as u32) as i64)
            .limit(page_size as i64)
            .load::<User>(conn)
            .await
            .map_err(From::from)
    }

    pub async fn total_users(&self, conn: &mut DbConnection) -> Result<u32, Error> {
        use crate::schema::users::{self, email};
        users::table
            .select(count(email))
            .first(conn)
            .await
            .map(|t: i64| t as u32)
            .map_err(From::from)
    }
}

#[cfg(test)]
pub mod test {
    use anyhow::Result;
    use diesel_async::AsyncConnection;

    use crate::{domain::user::model::NewUser, state::AppState};
    use common::value_object::password::{Password, Salt};

    #[tokio::test]
    async fn test() -> Result<()> {
        let state = AppState::new().await?;
        let mut conn = state.db.get().await?;
        conn.begin_test_transaction().await?;
        let email = "test@local.org".parse().unwrap();
        let salt = Salt::new();
        let password = "12345678".parse::<Password>().unwrap().hash(&salt)?;
        let user = state
            .user_service
            .add_user(
                NewUser {
                    email: &email,
                    password: &password,
                    salt: &salt,
                },
                &mut conn,
            )
            .await?;
        let lookup = state.user_service.find_by_email(&email, &mut conn).await?;
        assert!(lookup.is_some());
        let lookup = state.user_service.find_by_id(user.id, &mut conn).await?;
        assert!(lookup.is_some());

        Ok(())
    }
}
