use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use diesel_async::pooled_connection::bb8;
use serde_json::json;
use tracing::error;

use common::value_object::password::PasswordError;

use crate::domain;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error(transparent)]
    SessionDomain(#[from] Box<domain::session::error::Error>),

    #[error(transparent)]
    Anyhow(#[from] anyhow::Error),

    #[error("Invalid credentials")]
    InvalidCredentials,

    #[error(transparent)]
    SlatError(#[from] argon2::password_hash::Error),

    #[error(transparent)]
    PoolError(#[from] bb8::RunError),

    #[error(transparent)]
    Diesel(#[from] diesel::result::Error),

    #[error(transparent)]
    PasswordError(#[from] PasswordError),

    #[error("Session not found")]
    SessionNotFound,

    #[error("Session has no user linked with!")]
    UserNotFound,

    #[error("Cannot sign in while logged in, logout and re-try")]
    CannotSignInWhileLoggedIn,

    #[error("Email already used")]
    EmailAlreadyUsed,

    #[error(transparent)]
    Uuid(#[from] uuid::Error),
}

impl From<domain::session::error::Error> for Error {
    fn from(value: domain::session::error::Error) -> Self {
        Error::SessionDomain(Box::new(value))
    }
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        use Error::*;
        error!("session error, reason: {self}");
        let code = match self {
            InvalidCredentials | SessionNotFound | UserNotFound => StatusCode::UNAUTHORIZED,
            EmailAlreadyUsed => StatusCode::CONFLICT,
            CannotSignInWhileLoggedIn => StatusCode::METHOD_NOT_ALLOWED,
            Uuid(_) => StatusCode::BAD_REQUEST,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        };

        let body = json!({
            "code": code.as_u16(),
            "message": self.to_string(),
        });
        (code, Json(body)).into_response()
    }
}
