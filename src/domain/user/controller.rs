use axum::{debug_handler, extract::State, Json};
use axum_extra::extract::Query;

use super::{
    error::Error,
    model::{self, NewUser},
};
use crate::state::AppState;
use common::dto::*;
use common::value_object::password::Salt;

#[debug_handler]
pub async fn list_users(
    State(AppState {
        db, user_service, ..
    }): State<AppState>,
    _: model::User,
    Query(FetchUsers {
        mut page,
        page_size,
    }): Query<FetchUsers>,
) -> Result<Json<UsersList>, Error> {
    let mut conn = db.get().await?;
    if page == 0 {
        page += 1;
    }
    let users = user_service
        .get(&mut conn, page - 1, page_size)
        .await?
        .into_iter()
        .map(From::from)
        .collect();
    let total = user_service.total_users(&mut conn).await?;
    Ok(Json(UsersList { users, total }))
}

#[debug_handler]
pub async fn sign_in(
    State(AppState {
        db, user_service, ..
    }): State<AppState>,
    user: Option<model::User>,
    Json(SignInUser { email, password }): Json<SignInUser>,
) -> Result<Json<User>, Error> {
    if user.is_some() {
        return Err(Error::CannotSignInWhileLoggedIn);
    }

    let salt = Salt::default();
    let password = password.hash(&salt)?;
    let mut conn = db.get().await?;
    let user = user_service
        .add_user(
            NewUser {
                email: &email,
                password: &password,
                salt: &salt,
            },
            &mut conn,
        )
        .await?;
    Ok(Json(user.into()))
}
