use anyhow::Context;
use axum::{
    async_trait, debug_handler,
    extract::{FromRef, FromRequestParts, State},
    headers::{authorization::Bearer, Authorization},
    http::request::Parts,
    Json, TypedHeader,
};
use axum_extra::extract::Cached;
use uuid::Uuid;

use diesel::prelude::*;

use serde::Serialize;

use crate::{
    database::DbPool,
    domain::{
        permission::model::Permission,
        session::{model::Session, service::SessionService},
    },
    schema::{users, users_permissions},
};

use common::value_object::{
    email::Email,
    password::{HashedPassword, Salt},
};

use super::{error::Error, service::UserService};

#[derive(Clone, Debug, PartialEq, Queryable, Selectable, Identifiable, Eq, Serialize)]
#[diesel(table_name = users, primary_key(id))]
pub struct User {
    pub id: i32,
    pub email: Email,
    pub password: HashedPassword,
    pub salt: Salt,
}

impl From<User> for common::dto::User {
    fn from(value: User) -> Self {
        common::dto::User { email: value.email }
    }
}

#[derive(Identifiable, Selectable, Queryable, Associations, Debug)]
#[diesel(
    table_name = users_permissions,
    belongs_to(User),
    belongs_to(Permission),
    primary_key(user_id, permission_id)
)]
pub struct GroupPermission {
    user_id: i32,
    permission_id: i32,
}

#[derive(Debug, Insertable)]
#[diesel(table_name = users)]
pub struct NewUser<'a> {
    pub email: &'a Email,
    pub password: &'a HashedPassword,
    pub salt: &'a Salt,
}

#[async_trait]
impl<S> FromRequestParts<S> for User
where
    S: Send + Sync,
    DbPool: FromRef<S>,
    SessionService: FromRef<S>,
    UserService: FromRef<S>,
{
    type Rejection = Error;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let session = Cached::<Session>::from_request_parts(parts, state).await?;
        let pool = DbPool::from_ref(state);
        let mut conn = pool.get().await?;

        let user_service = UserService::from_ref(state);
        let user = user_service
            .find_by_id(session.user_id, &mut conn)
            .await?
            .ok_or(Error::UserNotFound)?;

        Ok(user)
    }
}

#[cfg(test)]
pub mod tests {
    use std::str::FromStr;

    use anyhow::Result;

    use crate::{database, domain::user::model::NewUser, schema};
    use common::value_object::{
        email::Email,
        password::{Password, Salt},
    };
    use diesel::insert_into;
    use diesel_async::{AsyncConnection, RunQueryDsl};

    #[tokio::test]
    pub async fn insert_user() -> Result<()> {
        use schema::users;

        let pool = database::connect().await?;
        let mut conn = pool.get().await?;
        // conn.begin_test_transaction().await?;

        let email = Email::from_str("test10@local.org").unwrap();
        let salt = Salt::new();
        let password = Password::from_str("12345678").unwrap().hash(&salt)?;

        insert_into(users::table)
            .values(NewUser {
                email: &email,
                password: &password,
                salt: &salt,
            })
            .execute(&mut conn)
            .await?;

        Ok(())
    }
}
