use diesel::prelude::*;

use serde::Serialize;

use crate::schema::permissions;

#[derive(Clone, Debug, PartialEq, Queryable, Selectable, Identifiable, Eq, Serialize)]
#[diesel(table_name = permissions, primary_key(id))]
pub struct Permission {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
}
