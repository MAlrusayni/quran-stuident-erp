use diesel::prelude::*;
use diesel_async::RunQueryDsl;
use uuid::Uuid;

use crate::database::DbConnection;

use super::{error::Error, model::Session};

#[derive(Clone)]
pub struct SessionService;

impl SessionService {
    pub async fn add_new_session(
        &self,
        user_id: i32,
        conn: &mut DbConnection,
    ) -> Result<Session, Error> {
        use crate::schema::sessions;
        let token = Uuid::new_v4();

        diesel::insert_into(sessions::table)
            .values(Session { user_id, token })
            .returning(Session::as_select())
            .get_result(conn)
            .await
            .map_err(From::from)
    }

    pub async fn find_by_token(
        &self,
        value: Uuid,
        conn: &mut DbConnection,
    ) -> Result<Option<Session>, Error> {
        use crate::schema::sessions::{self, *};

        sessions::table
            .select(Session::as_select())
            .filter(token.eq(value))
            .first(conn)
            .await
            .optional()
            .map_err(From::from)
    }

    pub async fn remove(&self, value: Uuid, conn: &mut DbConnection) -> Result<Session, Error> {
        use crate::schema::sessions::{self, *};

        diesel::delete(sessions::table)
            .filter(token.eq(value))
            .returning(Session::as_returning())
            .get_result(conn)
            .await
            .map_err(From::from)
    }
}

#[cfg(test)]
pub mod test {
    use anyhow::Result;
    use diesel_async::AsyncConnection;

    use crate::state::AppState;

    #[tokio::test]
    async fn test() -> Result<()> {
        let state = AppState::new().await?;
        let mut conn = state.db.get().await?;
        conn.begin_test_transaction().await?;
        // admin user id=1
        let session = state.session_service.add_new_session(1, &mut conn).await?;

        let lookup = state
            .session_service
            .find_by_token(session.token, &mut conn)
            .await?;
        assert!(lookup.is_some());

        Ok(())
    }
}
