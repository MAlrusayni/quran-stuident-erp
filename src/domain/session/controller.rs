use anyhow::Context;
use axum::{
    async_trait, debug_handler,
    extract::{FromRef, FromRequestParts, State},
    headers::{authorization::Bearer, Authorization},
    http::request::Parts,
    Json, TypedHeader,
};
use axum_extra::extract::{Cached, Form};

use common::{CurrentUser, LoginForm};
use serde::Deserialize;

use time::{OffsetDateTime, PrimitiveDateTime};
use tracing::debug;
use uuid::Uuid;

use super::error::Error;
use crate::{
    database::DbPool,
    domain::session::service::SessionService,
    domain::{
        session::model::Session,
        user::{model::User, service::UserService},
    },
    state::AppState,
};
use common::value_object::{email::Email, password::Password};

#[debug_handler]
pub async fn login(
    State(AppState {
        db,
        session_service,
        user_service,
        ..
    }): State<AppState>,
    Json(credential): Json<LoginForm>,
) -> Result<Json<CurrentUser>, Error> {
    let mut conn = db.get().await?;
    debug!("trying to login with {}", credential.email);
    let lookup_user = user_service
        .find_by_email(&credential.email, &mut conn)
        .await?
        .ok_or(Error::InvalidCredentials)?;
    debug!("found matching user");

    let hashed_password = credential.password.hash(&lookup_user.salt)?;

    if lookup_user.password != hashed_password {
        return Err(Error::InvalidCredentials);
    }

    debug!("adding new session for the user {}", lookup_user.email);
    let session = session_service
        .add_new_session(lookup_user.id, &mut conn)
        .await?;

    Ok(Json(CurrentUser {
        email: lookup_user.email,
        session: common::Session {
            token: session.token,
            since: OffsetDateTime::now_utc(),
        },
    }))
}

#[debug_handler]
pub async fn logout(
    State(AppState {
        db,
        session_service,
        ..
    }): State<AppState>,
    Cached(_user): Cached<User>,
    Cached(session): Cached<Session>,
) -> Result<(), Error> {
    let mut conn = db.get().await?;
    session_service.remove(session.token, &mut conn).await?;
    Ok(())
}
