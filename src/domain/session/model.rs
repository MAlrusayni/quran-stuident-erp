use anyhow::Context;
use axum::{
    async_trait, debug_handler,
    extract::{FromRef, FromRequestParts, State},
    headers::{authorization::Bearer, Authorization},
    http::request::Parts,
    Json, TypedHeader,
};
use axum_extra::extract::Form;

use common::{CurrentUser, LoginForm};
use serde::Deserialize;

use diesel::prelude::*;
use time::{OffsetDateTime, PrimitiveDateTime};
use tracing::debug;
use uuid::Uuid;

use crate::{
    database::DbPool,
    domain::user::{model::User, service::UserService},
    schema::sessions,
};

use super::{error::Error, service::SessionService};

#[derive(
    Clone, Debug, PartialEq, Eq, Queryable, Selectable, Identifiable, Associations, Insertable,
)]
#[diesel(table_name = sessions, primary_key(user_id, token), belongs_to(User))]
pub struct Session {
    pub user_id: i32,
    pub token: Uuid,
}

#[async_trait]
impl<S> FromRequestParts<S> for Session
where
    S: Send + Sync,
    DbPool: FromRef<S>,
    SessionService: FromRef<S>,
{
    type Rejection = Error;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        // check if session token in header
        let token = TypedHeader::<Authorization<Bearer>>::from_request_parts(parts, state)
            .await
            .context("failed to extract session token from authorization header")?
            .token()
            .parse::<Uuid>()
            .context("failed to parse session token into Uuid")?;

        let session_service = SessionService::from_ref(state);
        let pool = DbPool::from_ref(state);
        let mut conn = pool.get().await?;
        session_service
            .find_by_token(token, &mut conn)
            .await?
            .ok_or(Error::SessionNotFound)
    }
}
