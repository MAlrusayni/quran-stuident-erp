use anyhow::{Context, Result};
use diesel_async::{
    pooled_connection::{bb8, AsyncDieselConnectionManager},
    AsyncPgConnection,
};

pub type DbConnection = AsyncPgConnection;
pub type DbPool = bb8::Pool<DbConnection>;

pub async fn connect() -> Result<DbPool> {
    let config = AsyncDieselConnectionManager::<DbConnection>::new(
        "postgres://postgres:postgres@127.0.0.1:5432/dudee-app",
    );
    DbPool::builder()
        .build(config)
        .await
        .context("failed to build database pool")
}
