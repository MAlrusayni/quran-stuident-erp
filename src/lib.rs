pub mod database;
pub mod domain;
pub mod logging;
pub mod middleware;
pub mod router;
pub mod schema;
pub mod seeds;
pub mod state;

use std::net::SocketAddr;

use anyhow::Result;
use logging::setup_tracing;
use state::AppState;

pub async fn run() -> Result<()> {
    setup_tracing()?;
    let state = AppState::new().await?;

    // run seeds
    // seeds::run(state.db).await?;

    let router = router::axum_router(state);

    let addr = SocketAddr::from(([0, 0, 0, 0], 3030));
    axum::Server::bind(&addr)
        .serve(router.into_make_service())
        .await?;
    Ok(())
}
