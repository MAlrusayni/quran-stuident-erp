use anyhow::Result;
use tracing::Level;

pub fn setup_tracing() -> Result<()> {
    let level = envmnt::get_parse_or("DUDEE_LOG_LEVEL", Level::DEBUG)?;
    tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(level)
        .with_test_writer()
        .init();

    Ok(())
}
