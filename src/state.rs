use anyhow::Result;
use axum::extract::FromRef;

use crate::database::{self, DbPool};
use crate::domain::session::service::SessionService;
use crate::domain::user::service::UserService;

#[derive(Clone)]
pub struct AppState {
    pub db: DbPool,
    pub session_service: SessionService,
    pub user_service: UserService,
}

impl AppState {
    pub async fn new() -> Result<Self> {
        let session_service = SessionService;
        let user_service = UserService;
        Ok(AppState {
            db: database::connect().await?,
            session_service,
            user_service,
        })
    }
}

impl FromRef<AppState> for DbPool {
    fn from_ref(state: &AppState) -> Self {
        state.db.clone()
    }
}

impl FromRef<AppState> for SessionService {
    fn from_ref(state: &AppState) -> Self {
        state.session_service.clone()
    }
}

impl FromRef<AppState> for UserService {
    fn from_ref(state: &AppState) -> Self {
        state.user_service.clone()
    }
}
