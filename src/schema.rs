// @generated automatically by Diesel CLI.

diesel::table! {
    groups (id) {
        id -> Int4,
        name -> Varchar,
        description -> Nullable<Varchar>,
    }
}

diesel::table! {
    groups_members (group_id, user_id) {
        group_id -> Int4,
        user_id -> Int4,
    }
}

diesel::table! {
    groups_permissions (group_id, permission_id) {
        group_id -> Int4,
        permission_id -> Int4,
    }
}

diesel::table! {
    permissions (id) {
        id -> Int4,
        name -> Varchar,
        description -> Nullable<Varchar>,
    }
}

diesel::table! {
    sessions (user_id, token) {
        user_id -> Int4,
        token -> Uuid,
    }
}

diesel::table! {
    users (id) {
        id -> Int4,
        email -> Varchar,
        password -> Varchar,
        salt -> Varchar,
    }
}

diesel::table! {
    users_permissions (user_id, permission_id) {
        user_id -> Int4,
        permission_id -> Int4,
    }
}

diesel::allow_tables_to_appear_in_same_query!(
    groups,
    groups_members,
    groups_permissions,
    permissions,
    sessions,
    users,
    users_permissions,
);
