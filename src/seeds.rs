use anyhow::Result;
use std::str::FromStr;

use diesel::{dsl::exists, prelude::*, select};
use diesel_async::RunQueryDsl;

use crate::domain::group::model::{Group, NewGroup};
use crate::domain::user::model::{NewUser, User};
use crate::schema::{groups, users};

use crate::database::DbPool;
use common::value_object::{
    email::Email,
    password::{Password, Salt},
};

static ADMIN_EMAIL: &str = "muhannad.alrusayni@gmail.com";
static ADMIN_PASSWORD: &str = "12345678";

static ADMINS_GROUP: &str = "Admins";

pub async fn run(db: DbPool) -> Result<()> {
    let mut conn = db.get().await?;

    let is_seeded = {
        use crate::schema::users::*;

        select(exists(users::table.filter(email.eq(ADMIN_EMAIL))))
            .get_result::<bool>(&mut conn)
            .await?
    };

    if is_seeded {
        return Ok(());
    }

    let _admin_user = {
        let email = Email::from_str(ADMIN_EMAIL).unwrap();
        let salt = Salt::new();
        let password = Password::from_str(ADMIN_PASSWORD).unwrap().hash(&salt)?;

        diesel::insert_into(users::table)
            .values(NewUser {
                email: &email,
                password: &password,
                salt: &salt,
            })
            .get_result::<User>(&mut conn)
            .await?
    };

    let _admin_group = {
        diesel::insert_into(groups::table)
            .values(NewGroup {
                name: ADMINS_GROUP,
                description: None,
            })
            .get_result::<Group>(&mut conn)
            .await?
    };

    Ok(())
}
