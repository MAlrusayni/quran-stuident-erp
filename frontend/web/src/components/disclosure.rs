use std::{cell::RefMut, ops::Deref, rc::Rc};

use dioxus::prelude::*;
use dioxus_router::{use_router, Link};
use gloo::console::log;

#[derive(Clone, PartialEq)]
pub struct DisclosureState {
    pub open: bool,
}

#[derive(Props)]
pub struct DisclosureProps<'a, F> {
    #[props(into, optional)]
    class: Option<&'a str>,
    #[props(default = false)]
    initial_value: bool,
    builder: F,
}

#[allow(non_snake_case)]
pub fn Disclosure<'a, F>(cx: Scope<'a, DisclosureProps<'a, F>>) -> Element<'a>
where
    F: Fn(DisclosureState) -> LazyNodes<'a, 'a>,
{
    use_shared_state_provider(cx, || DisclosureState {
        open: cx.props.initial_value,
    });
    let state = {
        use_shared_state::<DisclosureState>(cx)
            .unwrap()
            .read()
            .clone()
    };
    let class = cx.props.class.unwrap_or("");
    render! {
        div {
            class: "{class}",
            (cx.props.builder)(state)
        }
    }
}

#[derive(Props)]
pub struct DisclosureButton<'a> {
    #[props(into, optional)]
    class: Option<&'a str>,
    #[props(into, optional)]
    onclick: Option<EventHandler<'a, (MouseEvent, bool)>>,
    children: Element<'a>,
}

#[allow(non_snake_case)]
pub fn DisclosureButton<'a>(cx: Scope<'a, DisclosureButton<'a>>) -> Element<'a> {
    let state = use_shared_state::<DisclosureState>(cx)
        .expect("DisclosureButton cannot be used outside of Disclosure context")
        .clone();
    let class = cx.props.class.unwrap_or("");
    render! {
        button {
            class: "{class}",
            onclick: move |event| {
                let val = { state.read().open };
                state.write().open = !val;
                if let Some(ref callback) = cx.props.onclick {
                    callback.call((event, !val));
                }
            },
            &cx.props.children
        }
    }
}

// #[derive(Clone, Props)]
// pub struct DisclosurePanelProps<'a> {
//     children: Element<'a>,
// }

// #[allow(non_snake_case)]
// pub fn DisclosurePanel(cx: Scope<DisclosurePanelProps>) -> Element {
//     (cx.props.render)(state.read().open)
// }
