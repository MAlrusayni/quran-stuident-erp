use anyhow::{anyhow, Context, Result};
use common::{
    dto::{FetchUsers, User, UsersList},
    CurrentUser, LoginForm,
};
use uuid::Uuid;

use crate::current_user::use_current_user;

pub struct Api;

impl Api {
    pub async fn login(form: &LoginForm) -> Result<CurrentUser> {
        let response = reqwest::Client::new()
            .post("http://localhost:3030/auth/login")
            .json(&form)
            .send()
            .await
            .context("Failed to login using API")?;

        match response.status().is_success() {
            true => response
                .json()
                .await
                .context("failed to deserialize login API response"),
            false => Err(anyhow!(
                "failed to login to system, reason {}",
                response
                    .text()
                    .await
                    .unwrap_or("failed to read response".to_owned())
            )),
        }
    }

    pub async fn logout() -> Result<()> {
        let current_user = use_current_user().ok_or(anyhow!("no user to logout"))?;
        let response = reqwest::Client::new()
            .post("http://localhost:3030/auth/logout")
            .bearer_auth(current_user.session.token)
            .send()
            .await
            .context("failed to logout from API")?;
        match response.status().is_success() {
            true => Ok(()),
            false => Err(anyhow!(
                "failed to logout from API with response: {}",
                response
                    .text()
                    .await
                    .unwrap_or("failed to read response".to_owned())
            )),
        }
    }

    pub async fn list_users(options: &FetchUsers) -> Result<UsersList> {
        let current_user = use_current_user().ok_or(anyhow!("no user to logout"))?;
        let response = reqwest::Client::new()
            .get("http://localhost:3030/")
            .bearer_auth(current_user.session.token)
            .query(&options)
            .send()
            .await
            .context("Failed to fetch list_users API")?;

        match response.status().is_success() {
            true => response
                .json()
                .await
                .context("failed to deserialize list_users response"),
            false => Err(anyhow!(
                "failed to fetch list_users, reason {}",
                response
                    .text()
                    .await
                    .unwrap_or("failed to read response".to_owned())
            )),
        }
    }
}
