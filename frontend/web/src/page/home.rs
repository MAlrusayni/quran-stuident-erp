use crate::current_user::{set_current_user, use_current_user};
use common::{
    dto::{FetchUsers, User},
    value_object::{email::Email, password::Password},
    LoginForm,
};
use dioxus::prelude::*;
use dioxus_router::{use_route, use_router, Redirect, Route, Router};
use formalize::{Form, Input};
use gloo::console::log;

use crate::{api::Api, layout::BaseLayout};

#[allow(non_snake_case)]
pub fn HomePage(cx: Scope) -> Element {
    if use_current_user().is_none() {
        return cx.render(rsx! {
            Redirect { to: "/auth/login" }
        });
    }

    cx.render(rsx! {
        BaseLayout {
            h1 { "Home page" }
        }
    })
}
