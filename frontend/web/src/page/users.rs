use crate::current_user::{set_current_user, use_current_user};
use common::{
    dto::{FetchUsers, User},
    value_object::{email::Email, password::Password},
    LoginForm,
};
use dioxus::prelude::*;
use dioxus_router::{use_route, use_router, Redirect, Route, Router};
use formalize::{Form, Input};
use gloo::console::log;

use crate::{api::Api, layout::BaseLayout};

#[allow(non_snake_case)]
pub fn UsersPage(cx: Scope) -> Element {
    if use_current_user().is_none() {
        return cx.render(rsx! {
            Redirect { to: "/auth/login" }
        });
    }

    let page = use_state(cx, || 1);
    let users = use_state::<Vec<User>>(cx, || vec![]);
    let error = use_state::<Option<String>>(cx, || None);
    let total = use_state(cx, || 0);

    use_effect(cx, (page,), |(page,)| {
        let users = users.clone();
        let error = error.clone();
        let total = total.clone();
        async move {
            let result = Api::list_users(&FetchUsers {
                page: *page,
                page_size: 10,
            })
            .await;
            match result {
                Ok(data) => {
                    users.set(data.users);
                    total.set(data.total);
                }
                Err(err) => error.set(Some(err.to_string())),
            }
        }
    });

    let empty_users = users.is_empty().then(|| {
        rsx! {
            p {
                class: "flex flex-grow flex-shrink p-1",
                "no users yet!"
            }
        }
    });

    let users_render = users.iter().map(|user| {
        rsx! {
            li {
                class: "even:bg-gray-50 hover:bg-gray-200 p-1",
                "{user.email}"
            }
        }
    });

    cx.render(rsx! {
        BaseLayout {
            div {
                class: "flex flex-col gap-2 flex-grow font-bold text-gray-800 mt-2",
                h1 {
                    class: "self-center",
                    "Accounts"
                }
                div {
                    class: "border border-gray-200 box-border flex-grow",
                    ul {
                        class: "",
                        empty_users
                        users_render
                    }
                }
                PageButtons { page: page.clone(), total: total.clone() }
            }
        }
    })
}

#[allow(non_snake_case)]
#[inline_props]
pub fn PageButtons(cx: Scope, page: UseState<u32>, total: UseState<u32>) -> Element {
    cx.render(rsx! {
        div {
            class: "flex gap-2 flex-shrink-0 flex-grow-0 m-2 mt-0",
            button {
                class: "rounded bg-blue-600 text-white p-2",
                onclick: move |_| if *page.get() > 1_u32 { page.set(page - 1) },
                "Previous"
            }
            "{page}"
            button {
                class: "rounded bg-blue-600 text-white p-2",
                onclick: move |_| page.set(page + 1),
                "Next"
            }
        }
    })
}
