use crate::current_user::{set_current_user, use_current_user};
use common::{
    value_object::{email::Email, password::Password},
    LoginForm,
};
use dioxus::prelude::*;
use dioxus_router::{use_route, use_router, Redirect, Route, Router};
use formalize::{Form, Input};
use gloo::console::log;

use crate::{api::Api, layout::BaseLayout};

#[allow(non_snake_case)]
pub fn LoginPage(cx: Scope) -> Element {
    let current_user = use_current_user();
    if current_user.is_some() {
        return cx.render(rsx! {
            Redirect { to: "/" }
        });
    }

    cx.render(rsx! {
        div {
            class: "flex w-full h-full items-center justify-center",
            div {
                class: "w-2/5 p-2 border rounded border-gray-400",
                Login { redirect: "/" }
            }
        }
    })
}

#[derive(Props)]
pub struct LoginProps<'a> {
    redirect: &'a str,
}

#[allow(non_snake_case)]
pub fn Login<'a>(cx: Scope<'a, LoginProps<'a>>) -> Element<'a> {
    let result = use_state(&cx, String::default);
    let router = use_router(cx);

    let login = move |login_form: LoginForm| {
        let redirect = cx.props.redirect.to_owned();
        cx.spawn({
            let result = result.to_owned();
            let router = router.clone();
            async move {
                let response = Api::login(&login_form).await;
                match response {
                    Ok(user) => {
                        set_current_user(user).unwrap();
                        router.navigate_to(&redirect);
                    }
                    Err(err) => result.set(format!("Error: {err}")),
                };
            }
        })
    };

    cx.render(rsx! {
        Form::<LoginForm> {
            class: "flex flex-col gap-2",
            on_valid: login,
            on_invalid: move |error| result.set(format!("{error:?}")),

            Input::<Email> {
                label: "Email",
                name: "email",
            }
            Input::<Password> {
                label: "Password",
                name: "password",
            }

            button {
                class: "rounded shadow-sm bg-sky-500 text-white border-none py-2 px-4",
                "type": "submit",

                "Login"
            }
        }
        div {
            class: "bg-sky-100 text-sky-500 p-4 rounded",

            "result: {result}"
        }
    })
}
