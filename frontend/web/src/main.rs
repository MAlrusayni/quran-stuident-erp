pub mod api;
pub mod components;
pub mod current_user;
pub mod layout;
pub mod page;

use common::{
    value_object::{email::Email, password::Password},
    LoginForm,
};
use current_user::{set_current_user, use_current_user};
use dioxus::prelude::*;
use dioxus_router::{use_route, use_router, Redirect, Route, Router};
use formalize::{Form, Input};
use gloo::console::log;

use crate::page::{home::HomePage, login::LoginPage, users::UsersPage};
use crate::{api::Api, layout::BaseLayout};

fn main() {
    // dioxus_hot_reload::hot_reload_init!();
    dioxus_web::launch(App)
}

#[allow(non_snake_case)]
fn App(cx: Scope) -> Element {
    cx.render(rsx! {
        Router {
            Route {
                to: "/",
                HomePage {}
            }
            Route {
                to: "/accounts",
                UsersPage {}
            }
            Route {
                to: "/auth/login",
                LoginPage {}
            }
        }
    })
}
