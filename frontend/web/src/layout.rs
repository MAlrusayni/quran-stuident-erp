use std::{ops::Deref, rc::Rc};

use dioxus::{html::b, prelude::*};
use dioxus_router::{use_router, Link};
use gloo::console::log;

use crate::{
    api::Api,
    components::disclosure::{Disclosure, DisclosureButton, DisclosureState},
    current_user::{delete_current_user, use_current_user},
};

#[allow(non_snake_case)]
#[inline_props]
pub fn BaseLayout<'a>(cx: Scope<'a>, children: Element<'a>) -> Element<'a> {
    cx.render(rsx! {
        div {
            class: "flex h-full w-full overflow-hidden overflow-y-auto",
            Menu {}
            div {
                class: "flex flex-grow flex-shrink-0",
                children
            }
        }
    })
}

#[allow(non_snake_case)]
pub fn Menu(cx: Scope) -> Element {
    let router = use_router(cx);
    let logout = move |_| {
        let router = router.clone();
        cx.spawn({
            async move {
                Api::logout().await.expect("failed to from API");
                delete_current_user();
                router.navigate_to("/auth/login");
            }
        })
    };
    let logout_link = use_current_user().is_some().then(|| {
        cx.render(rsx! {
            button {
                class: "text-left border-none mt-auto",
                onclick: logout,
                "Logout"
            }
        })
    });
    render! {
        div {
            class: "flex flex-col w-1/5 h-full flex-grow-0 flex-shrink-0 text-white bg-blue-700 gap-1 p-2",
            Link {
                to: "/"
                active_class: "font-bold",
                "Home"
            }
            Disclosure {
                builder: move |state: DisclosureState| {
                    let x = "text-white".to_owned() + state.open.then(|| " font-blod").unwrap_or("");
                    rsx! {
                        DisclosureButton {
                            class: "{x}",
                            onclick: move |_| {
                                router.navigate_to("/accounts")
                            }
                            "Accounts"
                        }
                        div {
                            class: "ms-1",
                            state.open.then(|| rsx! {
                                div {
                                    class: "",
                                    Link {
                                        to: "/accounts/new-account"
                                        active_class: "font-bold",
                                        "Create Account"
                                    }
                                }
                            })
                        }
                    }
                }
            }
            // Link {
            //     to: "/accounts"
            //     active_class: "font-bold",
            //     "Accounts"
            // }
            logout_link
        }
    }
}
