use common::CurrentUser;
use gloo_storage::{errors::StorageError, LocalStorage, Storage};

static STORAGE_KEY: &str = "current-user";

pub fn use_current_user() -> Option<CurrentUser> {
    LocalStorage::get(STORAGE_KEY).ok()
}

pub fn set_current_user(user: CurrentUser) -> Result<(), StorageError> {
    LocalStorage::set(STORAGE_KEY, user)
}

pub fn delete_current_user() {
    LocalStorage::delete(STORAGE_KEY);
}
