-- Your SQL goes here

create table
    if not exists groups (
        id serial primary key,
        name varchar(254) not null unique,
        description varchar(254)
    )