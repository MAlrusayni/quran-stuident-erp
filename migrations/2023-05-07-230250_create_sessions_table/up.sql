-- Your SQL goes here

create table
    if not exists sessions (
        user_id int not null,
        token uuid not null,
        primary key (user_id, token),
        constraint fk_user foreign key(user_id) references users(id) on delete cascade
    )