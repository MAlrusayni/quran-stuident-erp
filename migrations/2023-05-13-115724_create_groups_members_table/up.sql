-- Your SQL goes here

create table
    if not exists groups_members (
        group_id integer,
        user_id integer,
        primary key(group_id, user_id),
        constraint fk_group foreign key(group_id) references groups(id) on delete cascade,
        constraint fk_user foreign key(user_id) references users(id) on delete cascade
    )