-- Your SQL goes here

create table
    if not exists groups_permissions (
        group_id integer,
        permission_id integer,
        primary key(group_id, permission_id),
        constraint fk_group foreign key(group_id) references groups(id) on delete cascade,
        constraint fk_permission_id foreign key(permission_id) references permissions(id) on delete cascade
    )