-- Your SQL goes here

create table if not exists users (
    id serial primary key,
    email varchar(255) not null unique,
    password varchar(255) not null,
    salt varchar(255) not null
)