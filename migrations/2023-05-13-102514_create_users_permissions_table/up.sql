-- Your SQL goes here

create table
    if not exists users_permissions (
        user_id integer,
        permission_id integer,
        primary key(user_id, permission_id),
        constraint fk_user foreign key(user_id) references users(id) on delete cascade,
        constraint fk_permission_id foreign key(permission_id) references permissions(id) on delete cascade
    )