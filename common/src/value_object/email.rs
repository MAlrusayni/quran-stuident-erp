use std::str::FromStr;

use derive_more::Display;
use serde_with::{DeserializeFromStr, SerializeDisplay};
use validator::validate_email;

#[cfg(feature = "backend")]
use diesel::{
    backend::Backend, deserialize::FromSql, serialize::ToSql, sql_types, AsExpression, FromSqlRow,
};

/// Email value object
///
/// the value always in lowercase.
#[derive(Debug, Eq, PartialEq, Clone, Display, SerializeDisplay, DeserializeFromStr)]
#[cfg_attr(feature = "backend", derive(AsExpression, FromSqlRow))]
#[cfg_attr(feature = "backend", diesel(sql_type = sql_types::Text))]
pub struct Email(String);

impl Email {
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl FromStr for Email {
    type Err = EmailError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if !validate_email(s) {
            return Err(EmailError);
        }

        Ok(Email(s.to_lowercase()))
    }
}

#[derive(thiserror::Error, Debug)]
#[error("Invalid email value")]
pub struct EmailError;

#[cfg(feature = "backend")]
impl<DB> FromSql<sql_types::Text, DB> for Email
where
    DB: Backend,
    String: FromSql<sql_types::Text, DB>,
{
    fn from_sql(bytes: DB::RawValue<'_>) -> diesel::deserialize::Result<Self> {
        String::from_sql(bytes)?.parse().map_err(From::from)
    }
}

#[cfg(feature = "backend")]
impl<DB> ToSql<sql_types::Text, DB> for Email
where
    DB: Backend,
    String: ToSql<sql_types::Text, DB>,
{
    fn to_sql<'b>(
        &'b self,
        out: &mut diesel::serialize::Output<'b, '_, DB>,
    ) -> diesel::serialize::Result {
        self.0.to_sql(out)
    }
}
