use std::str::FromStr;

use argon2::{
    password_hash::{rand_core::OsRng, SaltString},
    Argon2, PasswordHasher,
};
use derive_more::Display;
use serde_with::{DeserializeFromStr, SerializeDisplay};

#[cfg(feature = "backend")]
use diesel::{
    backend::Backend, deserialize::FromSql, serialize::ToSql, sql_types, AsExpression, FromSqlRow,
};

/// Password value object
///
/// the value always in lowercase.
#[derive(Debug, Clone, Display, SerializeDisplay, DeserializeFromStr)]
pub struct Password(String);

impl Password {
    const MIN_LENGTH: u8 = 8;
    const MAX_LENGTH: u8 = 255;

    pub fn as_str(&self) -> &str {
        &self.0
    }

    pub fn hash(&self, salt: &Salt) -> Result<HashedPassword, PasswordError> {
        Argon2::default()
            .hash_password(self.0.as_bytes(), &salt.0)?
            .hash
            .ok_or(PasswordError::FailedToGetHashedPassword)
            .map(|val| val.to_string())
            .map(HashedPassword)
    }
}

impl FromStr for Password {
    type Err = PasswordError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.is_empty() {
            return Err(PasswordError::Empty);
        }

        if s.len() > Self::MAX_LENGTH.into() {
            return Err(PasswordError::Long);
        }

        if s.len() < Self::MIN_LENGTH.into() {
            return Err(PasswordError::Short);
        }

        Ok(Password(s.to_lowercase()))
    }
}

#[derive(thiserror::Error, Debug)]
pub enum PasswordError {
    #[error("invalid password, password is very short")]
    Short,
    #[error("invalid password, password is very long")]
    Long,
    #[error("invalid password, password cannot be empty")]
    Empty,

    #[error("failed to hash password, reason: {0}")]
    FailedToHash(#[from] argon2::password_hash::Error),

    #[error("failed to get hashed password")]
    FailedToGetHashedPassword,
}

#[derive(Clone, Debug, PartialEq, Eq, SerializeDisplay, Display)]
#[cfg_attr(feature = "backend", derive(AsExpression, FromSqlRow))]
#[cfg_attr(feature = "backend", diesel(sql_type = sql_types::Text))]
pub struct HashedPassword(String);

#[cfg(feature = "backend")]
impl<DB> FromSql<sql_types::Text, DB> for HashedPassword
where
    DB: Backend,
    String: FromSql<sql_types::Text, DB>,
{
    fn from_sql(bytes: DB::RawValue<'_>) -> diesel::deserialize::Result<Self> {
        String::from_sql(bytes).map(HashedPassword)
    }
}

#[cfg(feature = "backend")]
impl<DB> ToSql<sql_types::Text, DB> for HashedPassword
where
    DB: Backend,
    String: ToSql<sql_types::Text, DB>,
{
    fn to_sql<'b>(
        &'b self,
        out: &mut diesel::serialize::Output<'b, '_, DB>,
    ) -> diesel::serialize::Result {
        self.0.to_sql(out)
    }
}

#[derive(Clone, Debug, Eq, PartialEq, SerializeDisplay, Display)]
#[cfg_attr(feature = "backend", derive(AsExpression, FromSqlRow))]
#[cfg_attr(feature = "backend", diesel(sql_type = sql_types::Text))]
pub struct Salt(SaltString);

impl Salt {
    pub fn new() -> Self {
        Self(SaltString::generate(OsRng))
    }
}

impl Default for Salt {
    fn default() -> Self {
        Self::new()
    }
}

impl FromStr for Salt {
    type Err = argon2::password_hash::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(SaltString::from_b64(s)?))
    }
}

#[cfg(feature = "backend")]
impl<DB> FromSql<sql_types::Text, DB> for Salt
where
    DB: Backend,
    String: FromSql<sql_types::Text, DB>,
{
    fn from_sql(bytes: DB::RawValue<'_>) -> diesel::deserialize::Result<Self> {
        let val = String::from_sql(bytes)?;
        let val = SaltString::from_b64(&val)?;
        Ok(Self(val))
    }
}

#[cfg(feature = "backend")]
impl<DB> ToSql<sql_types::Text, DB> for Salt
where
    DB: Backend,
    str: ToSql<sql_types::Text, DB>,
{
    fn to_sql<'b>(
        &'b self,
        out: &mut diesel::serialize::Output<'b, '_, DB>,
    ) -> diesel::serialize::Result {
        self.0.as_str().to_sql(out)
    }
}
