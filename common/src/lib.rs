pub mod value_object;

#[cfg(feature = "frontend")]
use formalize::FormData;
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;
use uuid::Uuid;
use value_object::{email::Email, password::Password};

pub mod dto {
    use serde::{Deserialize, Serialize};

    use crate::value_object::{email::Email, password::Password};

    #[derive(Debug, Serialize, Deserialize)]
    pub struct UsersList {
        pub users: Vec<User>,
        pub total: u32,
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct User {
        pub email: Email,
    }

    #[derive(Deserialize, Serialize)]
    pub struct SignInUser {
        pub email: Email,
        pub password: Password,
    }

    #[derive(Deserialize, Serialize)]
    pub struct FetchUsers {
        pub page: u32,
        pub page_size: u8,
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CurrentUser {
    pub email: Email,
    pub session: Session,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Session {
    pub token: Uuid,
    pub since: OffsetDateTime,
}

#[derive(Debug, Deserialize, Serialize)]
#[cfg_attr(feature = "frontend", derive(FormData))]
pub struct LoginForm {
    pub email: Email,
    pub password: Password,
}
